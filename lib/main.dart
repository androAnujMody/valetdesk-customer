import 'package:flutter/material.dart';
import 'package:valetdesk_customer/signUp.dart';

void main() => runApp(ValetApp());

class ValetApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "ValetDesk Customer",
      home: new SignUp(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.purple[500]
      ),
    );
  }
}
