import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:valetdesk_customer/utils/constants.dart';

class SignUp extends StatelessWidget {
  Future<SharedPreferences> preferences = SharedPreferences.getInstance();
  final nameController = TextEditingController();
  final numberController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  _saveData(String name, String number) async {
    if (formKey.currentState.validate()) {
      print("valid");
      formKey.currentState.save();
      SharedPreferences pref = await preferences;
      pref.setString(Constants.name, name);
      pref.setString(Constants.number, number);

      print(pref.getString(Constants.name));
      print(pref.getString(Constants.number));
    } else {
      print("invalid");
      GlobalKey<ScaffoldState>()
          .currentState
          .showSnackBar(SnackBar(content: Text("Error")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign Up"),
      ),
      body: Form(
        key: formKey,
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'Name required';
                    }
                  },
                  controller: nameController,
                  style: TextStyle(fontSize: 18.0, color: Colors.black),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      labelText: "Name", border: OutlineInputBorder()),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: TextFormField(
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Phone number required';
                      }
                    },
                    controller: numberController,
                    style: TextStyle(fontSize: 18.0, color: Colors.black),
                    keyboardType: TextInputType.phone,
                    maxLength: 10,
                    decoration: InputDecoration(
                        labelText: "Phone Number",
                        border: OutlineInputBorder()),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    onPressed: () {
                      _saveData(nameController.value.text,
                          numberController.value.text);
                    },
                    color: Colors.orangeAccent,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Center(
                        child: Text(
                          "Proceed",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
